package com.iron.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @copy 上海长江职业技能培训学校
 * @author 郭成杰
 * @des 官网启动器
 */
@SpringBootApplication
@MapperScan("com.iron.web.mapper")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}


