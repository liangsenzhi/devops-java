package com.iron.web.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author iron
 * @since 2020-08-28
 */
@Controller
@RequestMapping("/ddcheckusers")
public class DdcheckusersController {

}
