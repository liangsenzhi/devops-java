package com.iron.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.iron.web.entity.Ddcheckusers;
import com.iron.web.service.DdcheckusersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guocjsh
 * @des 页面跳转
 */
@RestController
public class IndexController {

    @Autowired
    private DdcheckusersService DdcheckusersService;

    @GetMapping("test")
   public String test(){
       return "{ \"code\": 20000,\n" +
               "    \"msg\": \"请求docker成功\",\n" +
               "    \"data\": }";

   }


    @GetMapping("getUser")
    public String test(@RequestParam  String username){
        QueryWrapper<Ddcheckusers> qw=new QueryWrapper<Ddcheckusers>();
        qw.lambda().eq(Ddcheckusers :: getUsername,username);
        Ddcheckusers user= DdcheckusersService.getOne(qw);
        return user.toString();

    }






}
