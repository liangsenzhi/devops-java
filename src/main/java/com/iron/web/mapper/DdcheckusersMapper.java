package com.iron.web.mapper;

import com.iron.web.entity.Ddcheckusers;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author iron
 * @since 2020-08-28
 */
public interface DdcheckusersMapper extends BaseMapper<Ddcheckusers> {

}
