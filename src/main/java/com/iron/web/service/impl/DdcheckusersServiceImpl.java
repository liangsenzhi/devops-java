package com.iron.web.service.impl;

import com.iron.web.entity.Ddcheckusers;
import com.iron.web.mapper.DdcheckusersMapper;
import com.iron.web.service.DdcheckusersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author iron
 * @since 2020-08-28
 */
@Service
public class DdcheckusersServiceImpl extends ServiceImpl<DdcheckusersMapper, Ddcheckusers> implements DdcheckusersService {

}
