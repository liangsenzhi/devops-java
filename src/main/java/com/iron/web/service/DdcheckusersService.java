package com.iron.web.service;

import com.iron.web.entity.Ddcheckusers;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author iron
 * @since 2020-08-28
 */
public interface DdcheckusersService extends IService<Ddcheckusers> {

}
