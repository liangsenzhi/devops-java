#设置基础镜像
FROM openjdk:8
#作者信息
MAINTAINER guocjsh (guocjsh@sinosoft.com.cn)
LABEL app="dockerweb" version="0.0.1" by="guocjsh"
COPY ./target/smart-web-0.0.1-SNAPSHOT.jar dockerweb.jar
#指定容器启动后要干的事
CMD java -jar dockerweb.jar